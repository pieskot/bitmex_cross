import os
import time
from datetime import date, timedelta, datetime

red = '\033[91m'
green = '\033[92m'
black = '\033[0m'
yellow = '\x1b[1;33;40m'
blue = '\x1b[1;34;40m'


def cls_2():
    print("\033c")


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def line():
    line = '==== ==== ==== ==== ==== ==== ==== ===='
    return line


def green_color(in_string):
    out_string = (green + in_string + black)
    return out_string


def red_color(in_string):
    out_string = (red + in_string + black)
    return out_string


def yellow_color(in_string):
    out_string = (yellow + in_string + black)
    return out_string


def blue_color(in_string):
    out_string = (blue + in_string + black)
    return out_string


def red_green(number):
    in_string = str(number)
    if number >= 0:
        out_string = (green + in_string + black)
    else:
        out_string = (red + in_string + black)
    return out_string


def stoch_red_green(number):
    in_string = str(number)
    if number >= 80:
        out_string = (red + in_string + black)
    elif number <= 20:
        out_string = (green + in_string + black)
    else:
        out_string = in_string
    return out_string


def tz_red_green(tz, city):
    hour = tz.strftime('%H')
    hour = int(hour)
    in_string = (city + ': ' + tz.strftime('%H:%M'))
    if hour <= 7 or hour >= 22:
        out_string = (red + in_string + black)
    else:
        out_string = (green + in_string + black)
    return out_string


def yellow_white(number):
    in_string = str(number)
    if number != 0:
        out_string = (yellow + in_string + black)
    else:
        out_string = in_string
    return out_string

def perc(up, down):
    if down != 0:
        perc = round((up / down - 1) * 100, 2)
    else:
        perc = 0
    return perc


def ma(number, list):
    ma = 0
    list_len = len(list)
    if list_len >= number and number > 0:
        for i in range(number):
            ma = ma + list[i]
        ma = ma / number
    return ma


def ema(number, list):
    ema = 0
    list_len = len(list)
    if list_len >= number and number > 0:
        alfa = 2 / (number + 1)
        up = 0
        down = 0
        for i in range(number):
            up = up + (((1 - alfa) ** i) * list[i])
            down = down + ((1 - alfa) ** i)
        if down > 0:
            ema = up / down
    return ema


def stochastic(close, low_list, high_list):
    lowest = min(low_list)
    highest = max(high_list)
    if highest != lowest:
        stochastic = round(((close - lowest) / (highest - lowest)) * 100, 2)
    else:
        stochastic = 0
    return stochastic

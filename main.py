import time
from datetime import date, timedelta, datetime
import utils_bitmex
import utils_constants
import utils_cl_gui
import utils_technical_analysis

contract_list = ['XBTU19', 'ADAU19', 'EOSU19', 'TRXU19', 'XRPU19']

rest = {}

for contract in contract_list:
    rest[contract] = utils_bitmex.Operator_REST(contract)
    rest[contract].update_leverage()

utils_cl_gui.cls_2()

while True:
    print(utils_cl_gui.yellow_color(utils_cl_gui.line()))
    now_time = datetime.now()
    print(str(now_time) + '\n')
    hour = int(now_time.strftime("%H"))
    minute = int(now_time.strftime("%M"))
    for contract in contract_list:
        try:
            rest[contract].get_position()
            rest[contract].get_trade_bucketed('1d', (utils_constants.LONG + 2))
            contract_data = True
        except Exception as e:
            contract_data = False
            print(utils_cl_gui.red_color(str(e)))

        if contract_data:
            ema_long = utils_technical_analysis.ema(
                utils_constants.LONG, rest[contract].ohlc['close'][1:])
            ema_short = utils_technical_analysis.ema(
                utils_constants.SHORT, rest[contract].ohlc['close'][1:])
            ema_long_last = utils_technical_analysis.ema(
                utils_constants.LONG, rest[contract].ohlc['close'][2:])
            ema_short_last = utils_technical_analysis.ema(
                utils_constants.SHORT, rest[contract].ohlc['close'][2:])
            ema_long_next = utils_technical_analysis.ema(
                utils_constants.LONG, rest[contract].ohlc['close'][0:])
            ema_short_next = utils_technical_analysis.ema(
                utils_constants.SHORT, rest[contract].ohlc['close'][0:])
            ema_perc = round((ema_short / ema_long - 1) * 100, 2)
            ema_perc_last = round((ema_short_last / ema_long_last - 1) * 100, 2)
            ema_perc_next = round((ema_short_next / ema_long_next - 1) * 100, 2)
            close = rest[contract].ohlc['close'][1]
            close_next = rest[contract].ohlc['close'][0]
            last_min = min(rest[contract].ohlc['open'][2], rest[contract].ohlc['close'][2])
            last_max = max(rest[contract].ohlc['open'][2], rest[contract].ohlc['close'][2])
            close_next = rest[contract].ohlc['close'][0]
            emergency_min = rest[contract].ohlc['low'][1] * (1 - utils_constants.EMERGENCY_LEVEL)
            emergency_max = rest[contract].ohlc['high'][1] * (1 + utils_constants.EMERGENCY_LEVEL)
            if not contract[:3] == 'XBT':
                rest[contract].amount = round(rest[contract].amount_start / close)
            print(str(contract) + ' - position: ' +
                  str(rest[contract].position) + ' - amount: ' + str(rest[contract].amount))
            print('perc next: ' + utils_cl_gui.red_green(ema_perc_next) +
                  '  perc: ' + utils_cl_gui.red_green(ema_perc) +
                  '  perc last: ' + utils_cl_gui.red_green(ema_perc_last))
            print('close next: ' + str(close_next) + '  close: ' + str(close) + '  min last: ' +
                  str(last_min) + '  max last: ' + str(last_max) + '\n')

            try:
                # long close emergency
                if rest[contract].position > 0 and close_next < emergency_min:
                    rest[contract].new_order('Sell', abs(rest[contract].position))
                    print(utils_cl_gui.red_color('emergency close long'))
                    time.sleep(20)
                    rest[contract].get_position()
                # long close emergency
                elif rest[contract].position < 0 and close_next > emergency_max:
                    rest[contract].new_order('Buy', abs(rest[contract].position))
                    print(utils_cl_gui.red_color('emergency close short'))
                    time.sleep(20)
                    rest[contract].get_position()
                emergency_close_position = True
            except Exception as e:
                emergency_close_position = False
                print(utils_cl_gui.red_color(str(e)))

            if hour == 2 and minute > 0 and rest[contract].midnight:
                # sell
                try:
                    # long close
                    if rest[contract].position > 0 and close < last_min:
                        rest[contract].new_order('Sell', abs(rest[contract].position))
                        print(utils_cl_gui.yellow_color('close long'))
                    # short close
                    elif rest[contract].position < 0 and close > last_max:
                        rest[contract].new_order('Buy', abs(rest[contract].position))
                        print(utils_cl_gui.yellow_color('close short'))
                    close_position = True
                except Exception as e:
                    close_position = False
                    print(utils_cl_gui.red_color(str(e)))
                # buy
                try:
                    # long open
                    if rest[contract].position == 0 and ema_short > ema_long and ema_short_last < ema_long_last:
                        rest[contract].new_order('Buy', rest[contract].amount)
                        print(utils_cl_gui.yellow_color('open long'))
                    # short open
                    elif rest[contract].position == 0 and ema_short < ema_long and ema_short_last > ema_long_last:
                        rest[contract].new_order('Sell', rest[contract].amount)
                        print(utils_cl_gui.yellow_color('open short'))
                    open_position = True
                except Exception as e:
                    open_position = False
                    print(utils_cl_gui.red_color(str(e)))

                if close_position and open_position:
                    rest[contract].midnight = False

            if not rest[contract].midnight and hour == 23:
                rest[contract].midnight = True

    for i in range(120):
        print(str(120 - i) + ' ', end='', flush=True)
        time.sleep(1)
    print()

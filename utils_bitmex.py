import time
import bitmex
from bitmex_websocket import BitMEXWebsocket
import timeout_decorator
import utils_constants
import utils_bitmex_api_key
import json

timeout_limit = utils_constants.TIMEOUT_LIMIT


@timeout_decorator.timeout(timeout_limit)
def get_active_contracts():
    instrument_list = []
    client = bitmex.bitmex()
    instrument_response = client.Instrument.Instrument_getActive().result()
    for instrument in instrument_response[0]:
        if instrument['symbol'] not in utils_constants.BAN_INSTRUMENTS:
            instrument_list.append(instrument['symbol'])
    return instrument_list


class API_Counter:
    def __init__(self):
        self.rest_limit = int(utils_constants.REST_LIMIT * 0.9)
        self.ws_limit = int(utils_constants.WS_LIMIT * 0.9)
        self.rest_counter = []
        self.ws_counter = []

    def rest_limit_ok(self):
        ts_now = round(time.time())
        for ts in self.rest_counter:
            if ts < (ts_now - 60):
                self.rest_counter.remove(ts)
        limit_now = len(self.rest_counter)
        if limit_now < self.rest_limit:
            return True
        else:
            return False

    def rest_limit_add(self):
        self.rest_counter.append(round(time.time()))

    def ws_limit_ok(self):
        ts_now = round(time.time())
        for ts in self.ws_counter:
            if ts < (ts_now - 60 * 60):
                self.ws_counter.remove(ts)
        limit_now = len(self.ws_counter)
        if limit_now < self.ws_limit:
            return True
        else:
            return False

    def ws_limit_add(self):
        self.ws_counter.append(round(time.time()))


class Operator_REST:
    @timeout_decorator.timeout(timeout_limit)
    def __init__(self, contract):
        self.bitmex_key = utils_bitmex_api_key.APIKEY
        self.bitmex_secret = utils_bitmex_api_key.SECRET
        self.leverage = utils_constants.LEVERAGE
        self.contract = contract
        self.contract_price = 0
        if self.contract[:3] == 'XBT':
            self.amount_start = utils_constants.AMOUNT_BTC
            self.amount = self.amount_start
        else:
            self.amount_start = utils_constants.AMOUNT
            self.amount = 0
        self.midnight = True
        self.api_client = bitmex.bitmex(
            test=False, api_key=self.bitmex_key, api_secret=self.bitmex_secret)

    @timeout_decorator.timeout(timeout_limit)
    def update_leverage(self):
        return self.api_client.Position.Position_updateLeverage(
            symbol=self.contract, leverage=self.leverage).result()

    @timeout_decorator.timeout(timeout_limit)
    def new_order(self, side, orderQty):
        return self.api_client.Order.Order_new(
            symbol=self.contract, side=side, ordType='Market', orderQty=orderQty).result()

    def get_position(self):
        self.position_data = self.api_client.Position.Position_get(
            filter=json.dumps({'symbol': self.contract})).result()

        self.position = self.position_data[0][0]['currentQty']
        return self.position

    @timeout_decorator.timeout(timeout_limit)
    def get_trade_bucketed(self, binSize, count):
        trade_bucketed = self.api_client.Trade.Trade_getBucketed(
            binSize=binSize, symbol=self.contract, partial=True, count=count, reverse=True).result()
        self.ohlc = {}
        self.ohlc['open'] = []
        self.ohlc['high'] = []
        self.ohlc['low'] = []
        self.ohlc['close'] = []
        self.ohlc['volume'] = []
        for i in trade_bucketed[0]:
            self.ohlc['open'].append(i['open'])
            self.ohlc['high'].append(i['high'])
            self.ohlc['low'].append(i['low'])
            self.ohlc['close'].append(i['close'])
            self.ohlc['volume'].append(i['volume'])
        if len(self.ohlc['open']) == count:
            self.ohlc_timestamp = round(time.time())
        return self.ohlc


class Operator_WS:
    @timeout_decorator.timeout(timeout_limit)
    def __init__(self, contract):
        self.bitmex_key = utils_bitmex_api_key.APIKEY
        self.bitmex_secret = utils_bitmex_api_key.SECRET
        self.contract = contract
        self.bitmex_ws = BitMEXWebsocket(endpoint='wss://www.bitmex.com/realtime',
                                         symbol=self.contract, api_key=self.bitmex_key, api_secret=self.bitmex_secret)

    # get all ws data
    def bitmex_ws_all_data(self):
        self.bitmex_ws_prices()
        self.bitmex_ws_account_balance()
        self.bitmex_ws_order()
        self.bitmex_ws_position()

    # inside all data
    def bitmex_ws_account_balance(self):
        if len(self.bitmex_ws.data['margin']) > 0:
            self.bitmex_account_balance = self.bitmex_ws.data['margin'][0]['marginBalance']
        else:
            self.bitmex_account_balance = 0

    # inside all data
    def bitmex_ws_order(self):
        if len(self.bitmex_ws.data['order']) > 0:
            self.bitmex_order = self.bitmex_ws.data['order']
        else:
            self.bitmex_order = []

    # inside all data
    def bitmex_ws_position(self):
        if len(self.bitmex_ws.data['position']) > 0:
            if self.bitmex_ws.data['position'][0]['currentQty'] is not None:
                self.bitmex_position = self.bitmex_ws.data['position'][0]['currentQty']
            else:
                self.bitmex_position = 0
        if len(self.bitmex_ws.data['position']) > 0:
            if self.bitmex_ws.data['position'][0]['avgEntryPrice'] is not None:
                self.bitmex_avgEntryPrice = self.bitmex_ws.data['position'][0]['avgEntryPrice']
            else:
                self.bitmex_avgEntryPrice = 0

    # inside all data
    def bitmex_ws_prices(self):
        i = len(self.bitmex_ws.data['quote'])
        self.bitmex_ask = float(self.bitmex_ws.data['quote'][i - 1]['askPrice'])
        self.bitmex_bid = float(self.bitmex_ws.data['quote'][i - 1]['bidPrice'])
        self.bitmex_average = round((self.bitmex_ask + self.bitmex_bid) / 2, 8)
        self.price_ts = str(self.bitmex_ws.data['quote'][i - 1]['timestamp'])

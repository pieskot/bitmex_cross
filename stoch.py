import time
from datetime import date, timedelta, datetime
from pytz import timezone
import utils_bitmex
import utils_constants
from utils_technical_analysis import stochastic
import utils_cl_gui

contract = 'XBTUSD'
#  interval_list = ['1m', '5m', '1h', '1d']

rest = utils_bitmex.Operator_REST(contract)

utils_cl_gui.cls_2()

while True:
    print(utils_cl_gui.yellow_color(utils_cl_gui.line()))
    now_time = datetime.now()
    print(str(now_time) + '\n')
    try:
        d1_data = rest.get_trade_bucketed('1d', 720)
        h1_data = rest.get_trade_bucketed('1h', 720)
        m5_data = rest.get_trade_bucketed('5m', 720)
        contract_data = True
    except Exception as e:
        contract_data = False
        print(utils_cl_gui.red_color(str(e)))
    if contract_data:
        print(h1_data['close'][0])
        print()
        #  30 x d1
        stoch = stochastic(d1_data['close'][0], d1_data['low'][0:719], d1_data['high'][0:719])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('D30: ' + stoch)
        #  14 x d1
        stoch = stochastic(d1_data['close'][0], d1_data['low'][0:336], d1_data['high'][0:336])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('D14: ' + stoch)
        #  7 x d1
        stoch = stochastic(d1_data['close'][0], d1_data['low'][0:166], d1_data['high'][0:166])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('D7:  ' + stoch)
        #  72 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:72], h1_data['high'][0:72])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H72: ' + stoch)
        #  24 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:24], h1_data['high'][0:24])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H24: ' + stoch)
        #  12 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:12], h1_data['high'][0:12])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H12: ' + stoch)
        #  6 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:6], h1_data['high'][0:6])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H6:  ' + stoch)
        #  3 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:3], h1_data['high'][0:3])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H3:  ' + stoch)
        #  1 x h1
        stoch = stochastic(h1_data['close'][0], h1_data['low'][0:1], h1_data['high'][0:1])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('H1:  ' + stoch)
        '''
        #  1 x m15
        stoch = stochastic(m5_data['close'][0], m5_data['low'][0:3], m5_data['high'][0:3])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('M15: ' + stoch)
        #  1 x m5
        stoch = stochastic(m5_data['close'][0], m5_data['low'][0:1], m5_data['high'][0:1])
        stoch = round(stoch, 2)
        stoch = utils_cl_gui.stoch_red_green(stoch)
        print('M5:  ' + stoch)
        '''
        print()
    #  timezone
    tz = datetime.now(timezone('Asia/Hong_Kong'))
    print(utils_cl_gui.tz_red_green(tz, 'Hong_Kong'))
    tz = datetime.now(timezone('Asia/Tokyo'))
    print(utils_cl_gui.tz_red_green(tz, 'Tokyo'))
    tz = datetime.now(timezone('America/Los_Angeles'))
    print(utils_cl_gui.tz_red_green(tz, 'Los_Angeles'))
    tz = datetime.now(timezone('America/New_York'))
    print(utils_cl_gui.tz_red_green(tz, 'New_York'))
    tz = datetime.now(timezone('Europe/London'))
    print(utils_cl_gui.tz_red_green(tz, 'London'))
    print()
    for i in range(60):
        print(str(60 - i) + ' ', end='', flush=True)
        time.sleep(1)
    print()

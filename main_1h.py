import time
from datetime import datetime
import utils_bitmex
import utils_constants
import utils_cl_gui
import utils_technical_analysis

contract_list = ['XBTZ19', 'XBTU19', 'ADAU19', 'EOSU19', 'TRXU19', 'XRPU19']

LONG = 24
SHORT = 2

rest = {}

for contract in contract_list:
    rest[contract] = utils_bitmex.Operator_REST(contract)
    rest[contract].update_leverage()

utils_cl_gui.cls_2()

while True:
    print(utils_cl_gui.yellow_color(utils_cl_gui.line()))
    now_time = datetime.now()
    print(str(now_time) + '\n')
    hour = int(now_time.strftime("%H"))
    minute = int(now_time.strftime("%M"))
    for contract in contract_list:
        try:
            rest[contract].get_position()
            rest[contract].get_trade_bucketed('1h', (LONG + 2))
            contract_data = True
        except Exception as e:
            contract_data = False
            print(utils_cl_gui.red_color(str(e)))

        if contract_data:
            ema_long = utils_technical_analysis.ema(
                LONG, rest[contract].ohlc['close'][1:])
            ema_short = utils_technical_analysis.ema(
                SHORT, rest[contract].ohlc['close'][1:])
            ema_long_last = utils_technical_analysis.ema(
                LONG, rest[contract].ohlc['close'][2:])
            ema_short_last = utils_technical_analysis.ema(
                SHORT, rest[contract].ohlc['close'][2:])
            ema_long_next = utils_technical_analysis.ema(
                LONG, rest[contract].ohlc['close'][0:])
            ema_short_next = utils_technical_analysis.ema(
                SHORT, rest[contract].ohlc['close'][0:])

            ema_perc = utils_technical_analysis.perc(ema_short, ema_long)
            ema_perc_last = utils_technical_analysis.perc(ema_short_last, ema_long_last)
            ema_perc_next = utils_technical_analysis.perc(ema_short_next, ema_long_next)

            open = rest[contract].ohlc['open'][1]
            close = rest[contract].ohlc['close'][1]
            last_open = rest[contract].ohlc['open'][2]
            last_close = rest[contract].ohlc['close'][2]

            if not contract[:3] == 'XBT':
                rest[contract].amount = round(rest[contract].amount_start / close)

            print(str(contract) + ' - position: ' +
                  utils_cl_gui.yellow_white(rest[contract].position) +
                  ' - amount: ' + str(rest[contract].amount) +
                  ' - contract price: ' + str(rest[contract].contract_price))
            print('perc next: ' + utils_cl_gui.red_green(ema_perc_next) +
                  '  perc: ' + utils_cl_gui.red_green(ema_perc) +
                  '  perc last: ' + utils_cl_gui.red_green(ema_perc_last))

            try:
                # long close emergency
                if rest[contract].position > 0:
                    rest[contract].contract_price = max(
                        rest[contract].contract_price, rest[contract].ohlc['close'][0])
                    if (rest[contract].contract_price * 0.99) > rest[contract].ohlc['close'][0]:
                        rest[contract].new_order('Sell', abs(rest[contract].position))
                        print(utils_cl_gui.red_color('emergency close long ' +
                                                     str(rest[contract].ohlc['close'][0])))
                        rest[contract].contract_price = 0
                        time.sleep(20)
                        rest[contract].get_position()
                # short close emergency
                elif rest[contract].position < 0:
                    rest[contract].contract_price = min(
                        rest[contract].contract_price, rest[contract].ohlc['close'][0])
                    if (rest[contract].contract_price * 1.01) < rest[contract].ohlc['close'][0]:
                        rest[contract].new_order('Buy', abs(rest[contract].position))
                        print(utils_cl_gui.red_color('emergency close short ' +
                                                     str(rest[contract].ohlc['close'][0])))
                        rest[contract].contract_price = 0
                        time.sleep(20)
                        rest[contract].get_position()
                emergency_close_position = True
            except Exception as e:
                emergency_close_position = False
                print(utils_cl_gui.red_color(str(e)))

            if minute >= 0 and minute < 10 and rest[contract].midnight:
                print(utils_cl_gui.yellow_color('1h check'))
                # sell
                try:
                    # long close
                    if rest[contract].position > 0 and close < (open * 0.998) and last_close < (last_open * 0.998):
                        rest[contract].new_order('Sell', abs(rest[contract].position))
                        print(utils_cl_gui.yellow_color(
                            'close long ' + str(rest[contract].ohlc['close'][0])))
                        rest[contract].contract_price = 0
                        time.sleep(20)
                        rest[contract].get_position()
                    # short close
                    elif rest[contract].position < 0 and close > (open * 1.002) and last_close > (last_open * 1.002):
                        rest[contract].new_order('Buy', abs(rest[contract].position))
                        print(utils_cl_gui.yellow_color(
                            'close short ' + str(rest[contract].ohlc['close'][0])))
                        rest[contract].contract_price = 0
                        time.sleep(20)
                        rest[contract].get_position()
                    close_position = True
                except Exception as e:
                    close_position = False
                    print(utils_cl_gui.red_color(str(e)))
                # buy
                try:
                    # long open
                    if rest[contract].position == 0 and ema_short > ema_long and ema_short_last < ema_long_last:
                        rest[contract].new_order('Buy', rest[contract].amount)
                        rest[contract].contract_price = rest[contract].ohlc['close'][0]
                        print(utils_cl_gui.yellow_color(
                            'open long ' + str(rest[contract].contract_price)))
                    # short open
                    elif rest[contract].position == 0 and ema_short < ema_long and ema_short_last > ema_long_last:
                        rest[contract].new_order('Sell', rest[contract].amount)
                        rest[contract].contract_price = rest[contract].ohlc['close'][0]
                        print(utils_cl_gui.yellow_color(
                            'open short ' + str(rest[contract].contract_price)))
                    open_position = True
                except Exception as e:
                    open_position = False
                    print(utils_cl_gui.red_color(str(e)))

                if close_position and open_position:
                    rest[contract].midnight = False

            if not rest[contract].midnight and minute > 11:
                rest[contract].midnight = True
            print()

    print()
    for i in range(60):
        print(str(60 - i) + ' ', end='', flush=True)
        time.sleep(1)
    print()
